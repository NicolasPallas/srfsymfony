<?php
namespace SRF\mainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class GameController extends Controller
{
    public function indexAction($game)
    {   
        /* TODO : Get info BDD */
        $gameData = self::loadGameByName($game);
        
        /* TODO : Push info in to the rendre */
        return $this->render('SRFmainBundle:Game:index.html.twig', array(
            'game' => $game
        ));
    }

    public static function loadGameByName() {
        /* TODO : Query to get the game */
        
    }
}
