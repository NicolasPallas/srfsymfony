<?php

namespace SRF\mainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    public function indexAction()
    {
        return $this->render('SRFmainBundle:Home:index.html.twig');
    }
}
